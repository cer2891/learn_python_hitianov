def listt():
    a = []
    x = int(input("Введите число: "))
    a.append(x)
    x = int(input("Введите число: "))
    a.append(x)
    print(a)
    y = a.pop()
    print(f'a={a}')
    print(f'y is pop={y}')

    d = [x ** 2 for x in range(10)]
    print(f'Генератор={d}')

    f = [x ** 2 for x in d if x % 2 == 0]
    print(f'Отсеяли по модулу 2 и возвели в степень 2 = {f}')


def insert_sort(a: list) -> None:
    """
    сортировка списка с вставками
    :param a: список
    :return: ничего не возращаем
    """
    for top in range(len(a)):
        k = top
        while k > 0 and a[k - 1] > a[k]:
            a[k], a[k - 1] = a[k - 1], a[k]
            k -= 1


def choice_sort(a: list) -> None:
    """
    Сортировка списка методом выбора
    :param a: список
    :return: ничего не возращает
    """
    for pos in range(0, len(a)-1):
        for k in range(pos+1, len(a)):
            if a[k] < a[pos]:
                a[k], a[pos] = a[pos], a[k]



def bubble_sort(a: list) -> None:
    """
    Сортировка списка методом пузырка
    :param a: список
    :return: ничего не возращает
    """
    for bypass in range(1, len(a)):
        for k in range(0, len(a)-bypass):
            if a[k] > a[k+1]:
                a[k], a[k+1] = a[k+1], a[k]


def test_sort(sort_algorithm) -> None:
    """
    Тестирование функций сортировки
    :param sort_algorithm: функция алгоритма
    :return:
    """
    print(f'Сортировка списка а: {sort_algorithm.__doc__}')
    print("test case #1 ", end="")
    a = [4, 3, 2, 1, 5]
    a_sorted = [1, 2, 3, 4, 5]
    sort_algorithm(a)
    print('Ok' if a == a_sorted else 'Fail')

    print("test case #2 ", end="")
    a = list(range(10, 20)) + list(range(0, 10))
    a_sorted = list(range(20))
    sort_algorithm(a)
    print('Ok' if a == a_sorted else 'Fail')

    print("test case #3 ", end="")
    a = [4, 2, 4, 2, 1]
    a_sorted = [1, 2, 2, 4, 4]
    sort_algorithm(a)
    print('Ok' if a == a_sorted else 'Fail')
    print(' ')
