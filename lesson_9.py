

# Сортировка слиянием

def merge(a: list, b: list) -> list:
    """
    Слияние отсортированых массивов
    i - индекс массива а
    k - индекс массива b
    j - индекс массива с
    :param a:
    :param b:
    :return:
    """
    i = k = n = 0
    c = [0] * (len(a)+len(b))
    while i < len(a) and k < len(b):
        if a[i] <= b[k]:
            c[n] = a[i]
            i += 1
            n += 1
        else:
            c[n] = b[k]
            k += 1
            n += 1
    while i < len(a):
        c[n] = a[i]; i += 1; n +=1;
    while k < len(b):
        c[n] = b[k]; k += 1; n +=1;
    return c

# Рекурсивная функцая


def merge_sort(a: list) -> list:
    if len(a) <= 1:
        return
    middle = len(a)//2
    L = a[0:middle]
    R = a[middle:len(a)]
    merge_sort(L)
    merge_sort(R)
    C = merge(L, R)
    for i in range(len(a)):
        a[i] = C[i]


# Сортировка Тони Хора
# TODO  не работает, нужно разбиратся в чем проблема
def hoar_sort(a: list):
    if len(a) <= 1:
        return
    barrier = a[0]; L = []; M = []; R = []
    for x in a:
        if x < barrier:
            L.append(x)
        elif x == barrier:
            M.append(x)
        else:
            R.append(x)
            hoar_sort(L)
            hoar_sort(R)
    k = 0
    for x in L+R+M:
        a[k] = x; k += 1


def check_sorted(A: list, ascending = True):
    """
    Проверка отсортированности за 0(len(A)
    :param A: сам массив
    :param ascending: по умолчанию True повзрастанию
    :return:
    """
    flag = True; s = 2 * int(ascending) -1
    for i in range(0, len(A)-1):
        if s*A[i] > s*A[i+1]:
            flag = False
            break
    return flag
