import lesson_5
import lesson_6
import lesson_7
import lesson_8
import lesson_9


def hello_separated(name='World', separator='-'):
    print("Hello", name, sep=separator)


def factorize_number(x: int) -> int:
    """
    Раскладывает число на множители.
    Печатает их на экран.
    :param x: int
    :return: int
    """
    divisor = 2
    while x > 1:
        if x % divisor == 0:
            print(divisor)
            x //= divisor
        else:
            divisor += 1


def is_simple_number(x: int) -> bool:
    """
    Определяет, является ли число простым.
    Если простое то True , а нет то False
    :param x: int:
    :return bool:
    """
    divisor = 2
    while divisor < x:
        if x % divisor == 0:
            return True
        divisor += 1
    return False


if __name__ == '__main__':
    a = [2, 4, 6, 7, 3, 12, 5, 3, 2]
    lesson_9.merge_sort(a)
    print(*a)
    print(f"Отсортирован ? = {lesson_9.check_sorted(a)}")

# lesson_8.gen_bin(8)
#  lesson_8.generate_number(3,3)

# lesson_8.generate_permutations(4,4)


# lesson_7.matryoshka(5)
# print(lesson_7.matryoshka)
# lesson_7.fractal_rectangle([100, 100], [500, 100], [500, 500], [100, 500])
# while True:
#     print('1')
# print(lesson_7.factorial(5))
# print(lesson_7.gcd(2,7))
# print(lesson_7.pow(5, 3))


# lesson_6.test_sort(lesson_6.insert_sort)
# lesson_6.test_sort(lesson_6.choice_sort)
# lesson_6.test_sort(lesson_6.bubble_sort)


# class_massiv = lesson_5.Masiv()
# class_massiv.copy_b_a()
# class_massiv.get_massiv_a_b()
# print(class_massiv.array_search(17))
# print(f"c={class_massiv.invert_array()}")
# class_massiv.get_massiv_a_b()
# print(class_massiv.shift_left())
# print(class_massiv.shift_right())
