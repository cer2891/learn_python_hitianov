
def massiv_copy() -> None:
    b = [0] * 20
    a = [0] * 20
    for k in range(20):
        a[k] = k
    for k in range(20):
        b[k] = a[k]
    print(a)
    print(b)
    a[0]= 56
    print(a)
    print(b)
    return None


class Masiv():
    """
    Создание двух массив,копируем один в другой
    """
    def __init__(self):
        self.a = [0] * 20
        self.b = [0] * 20

    def get_massiv_a_b(self) -> None:
        return print(f"Massiv b={self.b} \n Massiv a={self.a}")

    def copy_b_a(self):
        for i in range(len(self.a)):
            self.a[i] = i+2
            self.b[i] = self.a[i]
        self.b[0] = 45

    def array_search(self, n: int) -> int:
        """
        Поиск числа в массиве a
        :param n: Нужное число есть  ?
        :return: Возрат индекса числа в массиве или возрат -1 если его нету
        """
        for i in range(len(self.a)):
            if self.a[i] == n :
                return i
        return -1

    def invert_array(self) -> list:
        """
        :return: Возращаем инвертированный список массива а
        """
        c = [0] * len(self.a)
        temp = len(self.a)
        for i in range(len(self.a)):
            c[temp-i-1] = self.a[i]
        return c

    def shift_left(self) -> list:
        """
        Сдвиг массива а влево
        :return: возрат массива со сдвигом влево
        """
        d = [0] * len(self.a)
        temp = self.a[0]
        for i in range(len(self.a)-1):
            d[i] = self.a[i+1]
        d[len(self.a)-1] = temp
        return d

    def shift_right(self) -> list:
        """
        сдвиг массива а влево
        :return: возрат сдвинутого списка а вправо
        """
        d = [0] * 20
        temp = self.a[len(self.a)-1]
        for i in range(len(self.a)-2, -1, -1):
            d[i+1] = self.a[i]
        d[0] = temp
        return d
