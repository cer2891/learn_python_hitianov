# рекурсия
import graphics as gr

window = gr.GraphWin("Russian game", 600, 600)
alpha = 0.2


def fractal_rectangle(A: list, B: list, C: list, D: list, deep=10):
    """
    рисуем прямоугольник рекурсивно
    :param A:
    :param B:
    :param C:
    :param D:
    :param deep:
    :return:
    """
    if deep < 1:
        return
    for M, N in [A, B], [B, C], [C, D], [D, A]:
        gr.Line(gr.Point(*M), gr.Point(*N)).draw(window)
    A1 = [A[0] * (1 - alpha) + B[0] * alpha, A[1] * (1 - alpha) + B[1] * alpha]
    B1 = [B[0] * (1 - alpha) + C[0] * alpha, B[1] * (1 - alpha) + C[1] * alpha]
    C1 = [C[0] * (1 - alpha) + D[0] * alpha, C[1] * (1 - alpha) + D[1] * alpha]
    D1 = [D[0] * (1 - alpha) + A[0] * alpha, D[1] * (1 - alpha) + A[1] * alpha]
    fractal_rectangle(A1, B1, C1, D1, deep - 1)


def factorial(n: int):
    """
    Подсчет факториала
    :param n:
    :return:
    """
    assert n >= 0, "Факториал отриц не определен"
    if n == 0:
        return 1
    return factorial(n - 1) * n


def matryoshka(n: int):
    """
    вложеность матрешки
    :param n:
    :return:
    """
    if n == 1:
        print("Матрешечка")
    else:
        print(f"Верх матрешки n={n}")
        matryoshka(n - 1)
        print(f"Низ матрешки n={n}")


def gcd(a: int, b: int) -> int:
    """
    расчет Эвклида
    :param a:
    :param b:
    :return:
    """
    return (a if b == 0 else gcd(b, a % b))


# быстрое возведение в степень
def pow(a: int, n: int) -> int:
    """
    функция возведение в степень
    """
    if n == 0:
        return 1
    elif n % 2 == 1:  # нечетные цифры
        return pow(a, n - 1) * a
    else:
        return pow(a ** 2, n // 2)  # четные цифры
