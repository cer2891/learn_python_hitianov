# Создание алгоритм генерации всех чисел для N-ричной системы счисления
# Алгоритм генерации всех перестановок N-ричной системы счисления

def gen_bin(M:int, prefix=""):
    """
    Генерация чисел размером М двоичного числа
    :param M: размер двоичного числа
    :param prefix:
    :return:
    """
    if M == 0:
        print(prefix)
        return
    gen_bin(M-1, prefix+"0")
    gen_bin(M-1, prefix="1")


def generate_number(N:int, M:int, prefix=None):
    """
    Генериует все числа ( с лидирующим незначащими нулями)
    в N-ричной системе счисления ( N <= 10)
    :param N: N <= 10
    :param M:
    :param prefix:
    :return:
    """
    prefix = prefix or []
    if M == 0:
        print(prefix)
        return
    for digit in range(N):
        prefix.append(digit)
        generate_number(N, M-1, prefix)
        prefix.pop()


def find(number, A):
    """
    ищет number в А и возращает True, если есть такой
    False если такого нет
    :param number:
    :param A:
    :return:
    """
    for x in A:
        if number == x:
            return True
    return False


def generate_permutations(N:int, M:int=-1, prefix=None):
    """
    Генерация всех перестановок N чисел в N позициях,
    с префиксом prefix
    :param N:
    :param M:
    :param prefix:
    :return:
    """
    M = N if M == -1 else M # по умолчанию N чисел в N позициях
    prefix = prefix or []
    if M == 0:
        print(*prefix, sep="")
        return
    for number in range(1, N+1):
        if find(number, prefix):
            continue
        prefix.append(number)
        generate_permutations(N, M-1, prefix)
        prefix.pop()
